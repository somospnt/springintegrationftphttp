# Integration FTP Http #

Este proyecto consta de dos flujos principales:

### El primero "lectorDeArchivoFtpFlow" lee archivos desde un ftp y los guarda en el fileSystem ###
Lo hace de forma recursiva

Filtra los archivos por nombre

Y persiste el nombre de los archivos que ya leyó en base de datos para no volver a procesarlos

### El segundo "cargaDeArchivosFlow" lee los archivos dejados por el ftp y los envía a través de http ###
Si la respuesta es 201 lo mueve a la carpeta configurada como procesado
Si la respuesta es otra lo mueve a la carpeta configurada como error

### Un tercer flow "informeErrorFlow" que ante cualquier excepción envía un mail con el mensaje ###

### Diagrama de flujos de Spring Integration ###

![diag2.png](https://bitbucket.org/repo/5BrqLL/images/749089961-diag2.png)