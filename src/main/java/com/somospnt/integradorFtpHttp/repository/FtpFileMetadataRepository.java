package com.somospnt.integradorFtpHttp.repository;

import com.somospnt.integradorFtpHttp.domain.FtpFileMetadata;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FtpFileMetadataRepository extends JpaRepository<FtpFileMetadata, String> {

}
