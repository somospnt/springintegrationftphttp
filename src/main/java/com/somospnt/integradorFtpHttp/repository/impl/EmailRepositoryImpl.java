package com.somospnt.integradorFtpHttp.repository.impl;

import com.somospnt.integradorFtpHttp.repository.EmailRepository;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class EmailRepositoryImpl implements EmailRepository {

    @Autowired
    private JavaMailSender mailSender;

    @Value(value = "${somos.pnt.mail.to}")
    private String to;
    @Value(value = "${somos.pnt.mail.from}")
    private String from;
    @Value(value = "${somos.pnt.mail.asunto}")
    private String asunto;

    @Override
    public void enviar(String msj) {
        MimeMessagePreparator preparator = (MimeMessage mimeMessage) -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setTo(to);
            message.setFrom(from);
            message.setText(msj);
            message.setSubject(asunto);
        };
        mailSender.send(preparator);
    }
}