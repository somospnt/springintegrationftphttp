package com.somospnt.integradorFtpHttp.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FtpFileMetadata {

    @Id
    private String nombre;
    private String modified;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

}