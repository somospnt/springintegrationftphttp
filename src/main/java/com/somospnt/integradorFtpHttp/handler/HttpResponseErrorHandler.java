package com.somospnt.integradorFtpHttp.handler;

import java.io.IOException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

public class HttpResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse chr) throws IOException {
        return false;
    }

    //Se sobreescribe para que no lanze exception
    @Override
    public void handleError(ClientHttpResponse chr) throws IOException {
    }

}
