package com.somospnt.integradorFtpHttp.handler;

import com.somospnt.integradorFtpHttp.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

@Component
public class InfomeErrorHandler implements MessageHandler {

    @Autowired
    private EmailRepository emailRepository;

    @Override
    public void handleMessage(Message<?> msg) throws MessagingException {
        emailRepository.enviar(msg.getPayload().toString());
    }

}
