package com.somospnt.integradorFtpHttp.filter;

import org.apache.commons.net.ftp.FTPFile;
import org.springframework.integration.file.filters.AbstractFileListFilter;
import org.springframework.stereotype.Component;

@Component
public class NombreFileFilter extends AbstractFileListFilter<FTPFile> {

    @Override
    protected boolean accept(FTPFile file) {
        String nombre = file.getName();
        //No toma en cuenta las carpetas por que si no, no entra dentro
        if (file.isDirectory()) {
            return true;
        }
        return nombre.contains("_RD") && (nombre.endsWith(".TXT") || nombre.endsWith(".txt"));
    }

}
