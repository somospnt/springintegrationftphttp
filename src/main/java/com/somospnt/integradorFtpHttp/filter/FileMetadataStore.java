package com.somospnt.integradorFtpHttp.filter;

import com.somospnt.integradorFtpHttp.domain.FtpFileMetadata;
import com.somospnt.integradorFtpHttp.repository.FtpFileMetadataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.metadata.ConcurrentMetadataStore;
import org.springframework.stereotype.Component;

/**
 *
 * Implementado usando la clase HashMap como base
 */
@Component
public class FileMetadataStore implements ConcurrentMetadataStore {

    @Autowired
    private FtpFileMetadataRepository ftpFileMetadataRepository;

    @Override
    public String putIfAbsent(String nombre, String modified) {
        FtpFileMetadata fileMetadata = ftpFileMetadataRepository.findOne(nombre);
        //No toma en cuenta las carpetas por que si no, no entra dentro
        if (fileMetadata == null || !hasExtensionTxt(nombre)) {
            this.put(nombre, modified);
            return null;
        } else {
            return fileMetadata.getModified();
        }
    }

    @Override
    public boolean replace(String nombre, String oldModified, String newModified) {
        FtpFileMetadata fileMetadata = ftpFileMetadataRepository.findOne(nombre);
        if (fileMetadata != null && fileMetadata.getModified().equals(oldModified)) {
            fileMetadata.setModified(newModified);
            ftpFileMetadataRepository.saveAndFlush(fileMetadata);
            return true;
        }
        return false;
    }

    @Override
    public void put(String nombre, String modified) {
        FtpFileMetadata fileMetadata = new FtpFileMetadata();
        fileMetadata.setNombre(nombre);
        fileMetadata.setModified(modified);
        ftpFileMetadataRepository.saveAndFlush(fileMetadata);
    }

    @Override
    public String get(String nombre) {
        return ftpFileMetadataRepository.findOne(nombre).getModified();
    }

    @Override
    public String remove(String nombre) {
        String modified = ftpFileMetadataRepository.findOne(nombre).getModified();
        ftpFileMetadataRepository.delete(nombre);
        return modified;
    }

    private boolean hasExtensionTxt(String nombre) {
        return nombre.endsWith(".TXT") || nombre.endsWith(".txt");
    }

}
