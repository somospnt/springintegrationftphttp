package com.somospnt.integradorFtpHttp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EnableIntegration
public class IntegradorFtpHttpApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegradorFtpHttpApplication.class, args);
    }
}
