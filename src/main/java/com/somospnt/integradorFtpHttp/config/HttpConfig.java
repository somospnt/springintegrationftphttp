package com.somospnt.integradorFtpHttp.config;

import java.io.File;
import com.somospnt.integradorFtpHttp.handler.HttpResponseErrorHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.dsl.support.Transformers;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.file.filters.RegexPatternFileListFilter;
import org.springframework.integration.file.support.FileExistsMode;
import org.springframework.integration.http.HttpHeaders;
import org.springframework.messaging.Message;

@Configuration
public class HttpConfig {

    @Value("${somos.pnt.local.file.dir.procesados}")
    private String pathArchivosProcesados;
    
    @Value("${somos.pnt.local.file.dir.error}")
    private String pathArchivosPorError;

    @Value("${somos.pnt.local.file.dir.recibidos}")
    private String pathArchivoRecibidos;

    @Value("${somos.pnt..url}")
    private String url;

    @Value("${somos.pnt.authorization.basic}")
    private String autorizationBasic;

    @Value("${somos.pnt.polling.fixed.rate}")
    private int pollingRate;

    @Bean
    public IntegrationFlow cargaDeArchivoFlow() {
        return IntegrationFlows
                .from(fileReadingMessageSource(), c -> c.poller(p -> p.fixedRate(pollingRate).maxMessagesPerPoll(-1)).autoStartup(true))
                .transform(Transformers.fileToByteArray())
                .enrichHeaders(h -> h.header("Authorization", autorizationBasic))
                .handle(Http.outboundGateway(url)
                        .expectedResponseType(String.class)
                        .httpMethod(HttpMethod.POST)
                        //Cambiamos el errorHandler para que lanze excepcion al recibir un codigo 4XX
                        .errorHandler(new HttpResponseErrorHandler())
                )
                //Rutea el mensaje a todo aquel que devuelva true en la expresion, por ejemplo estadoHttpEsCreado()
                .routeToRecipients(r -> r
                        .recipient("archivoProcesadoChannel",
                                m -> estadoHttpEsCreado(m))
                        .recipient("errorChannel",
                                m -> !estadoHttpEsCreado(m))
                        .recipient("archivoErrorChannel",
                                m -> !estadoHttpEsCreado(m))
                )
                .get();
    }

    private MessageSource<File> fileReadingMessageSource() {
        FileReadingMessageSource source = new FileReadingMessageSource();
        source.setDirectory(new File(pathArchivoRecibidos));
        source.setFilter(new RegexPatternFileListFilter(".*\\.TXT|.*\\.txt"));
        return source;
    }
    
    @Bean
    public IntegrationFlow moverArchivoProcesadoFlow() {
        return IntegrationFlows.from("archivoProcesadoChannel")
                .transform("headers['file_originalFile']")
                .handle(fileWritingMessageHandler(pathArchivosProcesados))
                .get();
    }
    
    @Bean
    public IntegrationFlow moverArchivoPorErrorFlow() {
        return IntegrationFlows.from("archivoErrorChannel")
                .transform("headers['file_originalFile']")
                .handle(fileWritingMessageHandler(pathArchivosPorError))
                .get();
    }

    private FileWritingMessageHandler fileWritingMessageHandler(String directorioDestino) {
        FileWritingMessageHandler handler = new FileWritingMessageHandler(new File(directorioDestino));
        handler.setFileExistsMode(FileExistsMode.APPEND);
        handler.setDeleteSourceFiles(true);
        handler.setExpectReply(false);
        return handler;
    }

    private boolean estadoHttpEsCreado(Message<?> msg) {
        return msg.getHeaders().get(HttpHeaders.STATUS_CODE).equals(HttpStatus.CREATED);
    }

}
