package com.somospnt.integradorFtpHttp.config;

import com.somospnt.integradorFtpHttp.filter.FileMetadataStore;
import com.somospnt.integradorFtpHttp.filter.NombreFileFilter;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.ftp.Ftp;
import org.springframework.integration.endpoint.MethodInvokingMessageSource;
import org.springframework.integration.file.remote.gateway.AbstractRemoteFileOutboundGateway;
import org.springframework.integration.file.remote.gateway.AbstractRemoteFileOutboundGateway.Command;
import org.springframework.integration.file.remote.gateway.AbstractRemoteFileOutboundGateway.Option;
import org.springframework.integration.file.support.FileExistsMode;
import org.springframework.integration.ftp.filters.FtpPersistentAcceptOnceFileListFilter;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.messaging.MessageHandler;

@Configuration
public class RecolectorArchivosFtpConfig {

    @Value("${somos.pnt.remote.host}")
    private String host;

    @Value("${somos.pnt.remote.port}")
    private int port;

    @Value("${somos.pnt.remote.user}")
    private String user;

    @Value("${somos.pnt.remote.password}")
    private String password;

    @Value("${somos.pnt.remote.file.dir}")
    private String directorioRemoto;

    @Value("${somos.pnt.local.file.dir.recibidos}")
    private String directorioArchivosRecibidos;

    @Value("${somos.pnt.polling.fixed.rate}")
    private int pollingRate;

    @Autowired
    private FileMetadataStore fileMetadataStore;

    @Autowired
    private NombreFileFilter nombreFileFilter;

    @Bean
    public IntegrationFlow lectorArchivoFtpFlow(DefaultFtpSessionFactory ftpSessionFactory) {
        return IntegrationFlows.from(integerMessageSource(), c -> c.poller(p -> p.fixedRate(pollingRate)).autoStartup(true))
                .handle(ftpOutboundGateway())
                .split("payload")
                .handle(loggingHandler())
                .get();
    }

    private MessageHandler ftpOutboundGateway() {        
        AbstractRemoteFileOutboundGateway<FTPFile> ftpOutbound = Ftp.outboundGateway(ftpSessionFactory(),
                Command.MGET, directorioRemoto)
                //Lee recursivamente
                .options(Option.RECURSIVE)
                .localDirectory(new File(directorioArchivosRecibidos))
                //Persiste en base de datos el nombre de los archivos ya leidos 
                .filter(new FtpPersistentAcceptOnceFileListFilter(fileMetadataStore, ""))
                //filtra por nombre de archivo
                .filter(nombreFileFilter)
                .autoCreateLocalDirectory(true)
                .get();
        //reemplaza si ya existe el archivo
        ftpOutbound.setFileExistsMode(FileExistsMode.REPLACE);
        return ftpOutbound;
    }

    @Bean
    public DefaultFtpSessionFactory ftpSessionFactory() {
        DefaultFtpSessionFactory dfsf = new DefaultFtpSessionFactory();
        dfsf.setHost(host);
        dfsf.setUsername(user);
        dfsf.setPassword(password);
        dfsf.setPort(port);
        dfsf.setClientMode(2);
        return dfsf;
    }

    private MessageSource<?> integerMessageSource() {
        MethodInvokingMessageSource source = new MethodInvokingMessageSource();
        source.setObject(new AtomicInteger());
        source.setMethodName("getAndIncrement");
        return source;
    }

    private MessageHandler loggingHandler() {
        LoggingHandler loggingHandler = new LoggingHandler("INFO");
        loggingHandler.setLoggerName("archivo transferido");
        return loggingHandler;
    }

}
