package com.somospnt.integradorFtpHttp.config;

import com.somospnt.integradorFtpHttp.handler.InfomeErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;


@Configuration
public class ErrorConfig {


    @Autowired
    private InfomeErrorHandler infomeErrorHandler;
    
    @Bean
    public IntegrationFlow informeErrorFlow() {
        return IntegrationFlows.from("errorChannel")
                .handle(infomeErrorHandler)
                .get();
    }


}
